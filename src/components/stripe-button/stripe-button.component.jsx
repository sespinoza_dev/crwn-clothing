import React from 'react'
import StripeCheckout from 'react-stripe-checkout'

const StripeCheckoutButton = ({ price }) => {
  const priceForStripe = price * 100
  const publishableKey = 'pk_test_51J7PuQAgmZfLHnaOTdJTEl21OWkq43mOM4OT4dW7rFvUb8gn2H23RYNYjwUXMX77hPvxZJO5QlQqTYiqcS8XWIvh00TvXNJC3n'
  const onToken = token => {
    console.log(token)
    alert('Payment Successful')
  }

  return (
    <StripeCheckout
      label='Pay Now'
      name='CRW Clothing Ltd.'
      billingAddress
      shippingAddress
      image='https:svgshare.com/i/CUz.svg'
      decription={`Your total is $${price}`}
      amount={priceForStripe}
      panelLabel='Pay Now'
      token={onToken}
      stripeKey={publishableKey}
    />
  )
}

export default StripeCheckoutButton
