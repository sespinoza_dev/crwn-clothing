import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyBALSYYFfi7f7_LisktVdydtMOdcEh85hc",
  authDomain: "crwn-db-3d3bf.firebaseapp.com",
  projectId: "crwn-db-3d3bf",
  storageBucket: "crwn-db-3d3bf.appspot.com",
  messagingSenderId: "14756254789",
  appId: "1:14756254789:web:2acc685b1f3d3a5e45ff2c"
}

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if(!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapShot = await userRef.get();

  if(!snapShot.exist) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      })
    } catch (error) {
      console.error('error creatin user', error.message);
    }
  }

  return userRef;
}


firebase.initializeApp(config);

export const addCollectionAndDocuments = async (collectionKey, objectsToAdd) => {
  const collectionRef = firestore.collection(collectionKey)

  const batch = firestore.batch()
  objectsToAdd.forEach(obj => {
    const newDocRef = collectionRef.doc()
    batch.set(newDocRef, obj)
  })

  return await batch.commit()
}

export const convertCollectionsSnapshotToMap = collections => {
  const transformedCollection = collections.docs.map(docSnapshot => {
    const { title, items } = docSnapshot.data()

    return {
      routeName: encodeURI(title.toLowerCase()),
      id: docSnapshot.id,
      title,
      items
    }
  })

  return transformedCollection.reduce((accumulator, collection) => {
    accumulator[collection.title.toLowerCase()] = collection
    return accumulator
  }, {})
}

export const auth = firebase.auth();
export const firestore = firebase.firestore();

// access to the provider class from the auth library
const provider = new firebase.auth.GoogleAuthProvider();

// always trigger the google pop up whenever we use this Google auth provider for authentication and sign in
provider.setCustomParameters({  propmt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
