import React, { useEffect } from 'react'
import { Route } from 'react-router-dom'
import { connect } from 'react-redux'

import CollectsionsOverviewContainer from '../../components/collections-overview/collections-overview.container'
import CollectionPageContainer from '../collection/collection.container'

import { fetchCollectionStartAsync } from '../../redux/shop/shop.actions'

const ShopPage = ({ fetchCollectionStartAsync, match }) => {

  useEffect(() => {
    fetchCollectionStartAsync()
  }, [fetchCollectionStartAsync])

  return(
    <div className="shop-page">
      <Route
        exact
        path={`${match.path}`}
        component={CollectsionsOverviewContainer}
      />
      <Route
        path={`${match.path}/:collectionId`}
        component={CollectionPageContainer}
      />
    </div>
  )
}

const mapDispatchToProps = dispatch => ({
  fetchCollectionStartAsync: () => dispatch(fetchCollectionStartAsync())
})

export default connect(
  null,
  mapDispatchToProps
)(ShopPage)
